package edu.upc.damo.toDoList;

import android.content.Context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Josep M on 20/10/2014.
 */
public class Model {

    private List<CharSequence> dades = new ArrayList<CharSequence>();


    Model(Context context){
        carregaDades(context);

    }

    private void carregaDades(Context c){
        String [] dadesEstatiques = c.getResources().getStringArray(R.array.dadesEstatiques);
        Collections.addAll(dades, dadesEstatiques);
    }


    public void add(CharSequence object) {
        dades.add(object);
    }



    public void del(int pos)
    {
        dades.remove(pos);
    }

    public int size() {
        return dades.size();
    }

    public Object getItem(int position) {
        return dades.get(position);
    }

    public long getItemId(int position) {
        return position;
    }
}
