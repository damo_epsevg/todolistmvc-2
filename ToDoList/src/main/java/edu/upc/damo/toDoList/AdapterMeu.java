package edu.upc.damo.toDoList;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by Josep M on 15/10/2014.
 */
public class AdapterMeu extends BaseAdapter {
    Model dades;

    Context c;
    int resource;       // Layout de fila
    int textResourceId;   // TextView dins del layout de fila on s'ha de visualitzar cada dada

    AdapterMeu(Context c, int resource, int textResourceId, Model dades) {
        this.c=c;
        this.resource = resource;
        this.textResourceId = textResourceId;
        this.dades = dades;
    }

    public void add(CharSequence object) {
        dades.add(object);
        notifyDataSetChanged();
    }

    public void del(int pos)
    {
        dades.del(pos);
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return dades.size();
    }

    @Override
    public Object getItem(int position) {
        return dades.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return dades.getItemId(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null)
        {
            convertView = ((Activity) c).getLayoutInflater().inflate(resource,null);
        }

        TextView textView = convertView.findViewById(textResourceId);
        String dada = dades.getItem(position).toString();
        textView.setText(dada);
        return convertView;
    }
}
